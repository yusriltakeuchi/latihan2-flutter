import 'package:flutter/material.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: RegisterBody(),
        ),
      ),
    );
  }
}

class RegisterBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.only(top: 100, left: 30, right: 30),
      child: Column(
        children: <Widget>[
          Text(
            "Register Akun User",
            style: TextStyle(
              color: Colors.black,
              fontSize: 25,
              fontWeight: FontWeight.bold
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: TextField(
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                hintText: "Username",
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: TextField(
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                hintText: "Email",
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: TextField(
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.go,
              decoration: InputDecoration(
                hintText: "Password",
              ),
              obscureText: true,
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: TextField(
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.go,
              decoration: InputDecoration(
                hintText: "Confirm Password",
              ),
              obscureText: true,
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top: 50),
            child: Container(
              width: MediaQuery.of(context).size.width - 50,
              height: 50,
              child: RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                color: Colors.blue,
                child: Text(
                  "REGISTER",
                  style: TextStyle(
                    fontSize: 17,
                    color: Colors.white,
                    fontWeight: FontWeight.bold
                  ),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7)
                ),
              ),
            ),
          )

        ],
      ),
    );
  }
}